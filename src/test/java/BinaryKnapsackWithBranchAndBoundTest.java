import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BinaryKnapsackWithBranchAndBoundTest {

    @Test(timeout = 10000)
    public void testcase01() {
        // Arrange
        int knapsackMaxWeight = 16;
        int[] gemsATK = new int[]{10, 40, 50, 30};
        int[] gemsWeight = new int[]{5, 2, 10, 5};

        assertTrue(gemsATK.length == gemsWeight.length);

        BinaryKnapsackWithBranchAndBound homework = new BinaryKnapsackWithBranchAndBound();

        // Act
        int output = homework.solve(gemsATK.length, gemsATK, gemsWeight, knapsackMaxWeight);

        // Assert
        assertEquals(90, output);
    }

    @Test(timeout = 10000)
    public void testcase02() {
        // Arrange
        int knapsackMaxWeight = 100;
        int[] gemsATK = new int[]{10, 40, 50, 30, 14, 30};
        int[] gemsWeight = new int[]{4, 9, 8, 5, 20, 17};

        assertTrue(gemsATK.length == gemsWeight.length);

        BinaryKnapsackWithBranchAndBound homework = new BinaryKnapsackWithBranchAndBound();

        // Act
        int output = homework.solve(gemsATK.length, gemsATK, gemsWeight, knapsackMaxWeight);

        // Assert
        assertEquals(174, output);
    }

    @Test(timeout = 10000)
    public void testcase03() {
        // Arrange
        int knapsackMaxWeight = 15;
        int[] gemsATK = new int[]{10, 40, 50, 30};
        int[] gemsWeight = new int[]{16, 20, 20, 16};

        assertTrue(gemsATK.length == gemsWeight.length);

        BinaryKnapsackWithBranchAndBound homework = new BinaryKnapsackWithBranchAndBound();

        // Act
        int output = homework.solve(gemsATK.length, gemsATK, gemsWeight, knapsackMaxWeight);

        // Assert
        assertEquals(0, output);
    }

}
