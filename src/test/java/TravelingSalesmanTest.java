import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TravelingSalesmanTest {

    @Test(timeout = 10000)
    public void testcase01() {
        // Arrange
        TravelingSalesman homework = new TravelingSalesman();
        int cityCount = 3;
        int[][] road = new int[][]{
                {0, 1, 5},
                {1, 0, 5},
                {1, 2, 10},
                {2, 1, 10},
                {0, 2, 7},
                {2, 0, 7}
        };

        // Act
        int ans = 22;
        int out = homework.solve(cityCount, road);

        // Assert
        assertEquals(ans, out);
    }

    @Test(timeout = 10000)
    public void testcase02() {
        // Arrange
        TravelingSalesman homework = new TravelingSalesman();
        int cityCount = 4;
        int[][] road = new int[][]{
                {0, 1, 10},
                {1, 0, 10},
                {1, 3, 25},
                {3, 1, 25},
                {3, 2, 30},
                {2, 3, 30},
                {0, 2, 15},
                {2, 0, 15},
                {1, 2, 35},
                {2, 1, 35}
        };

        // Act
        int ans = 80;
        int out = homework.solve(cityCount, road);

        // Assert
        assertEquals(ans, out);
    }

    @Test(timeout = 10000)
    public void testcase03() {
        // Arrange
        TravelingSalesman homework = new TravelingSalesman();
        int cityCount = 4;
        int[][] road = new int[][]{
                {0, 1, 2},
                {0, 2, 9},
                {0, 3, 10},
                {1, 0, 1},
                {1, 2, 6},
                {1, 3, 4},
                {2, 0, 15},
                {2, 1, 7},
                {2, 3, 8},
                {3, 0, 6},
                {3, 1, 3},
                {3, 2, 12}
        };

        // Act
        int ans = 21;
        int out = homework.solve(cityCount, road);

        // Assert
        assertEquals(ans, out);
    }
}
