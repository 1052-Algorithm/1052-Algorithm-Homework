/**
 * 作業03B - 背包問題(Branch and Bound)
 */
public class BinaryKnapsackWithBranchAndBound {

    /**
     * 使用 Branch and Bound 演算法，求出背包可以攜帶最高的攻擊力總和
     *
     * @param gemCount          寶石數量
     * @param gemsATK           每顆寶石的攻擊力
     * @param gemsWeight        每顆寶石的重量
     * @param knapsackMaxWeight 背包最高的耐重
     * @return 在背包耐重內，最高的寶石攻擊力總和
     */
    public int solve(int gemCount, int[] gemsATK, int[] gemsWeight, int knapsackMaxWeight) {
        // TODO: 請實作作業
        return 0;
    }
}
